import React from "react";
// import axios from "axios";
import API from "../api";

import "./LoginForm.css";

export default class LoginForm extends React.Component {
  state = {
    login: "",
    password: "",
  };

  handleChangeLogin = (event) => {
    this.setState({
      login: event.target.value,
    });
  };

  handleChangePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const user = {
      login: this.state.login,
      password: this.state.password,
    };

    //for debug
    console.log(user);

    API.post("/Login", { user }).then((res) => {
      console.log(res);
      console.log(res.data);
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Login:
            <input type="text" name="login" onChange={this.handleChangeLogin} />
          </label>
          <label>
            Password:
            <input type="password" name="password" onChange={this.handleChangePassword} />
          </label>
          <button type="submit">Login</button>
        </form>
      </div>
    );
  }
}
